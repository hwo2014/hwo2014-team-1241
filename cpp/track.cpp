#include "track.h"

#include <cmath>

namespace hwo
{

// TODO: Move to a common header
const double PI = std::atan(1) * 4;

double Piece::laneLength(const Lane& lane) const
{
    return length > 0.0 ? length : arcLength(lane);
}

double Piece::laneRadius(const Lane& lane) const
{
    if (angle < 0)
        return radius + lane.offCenterDistance;
    return radius - lane.offCenterDistance;
}

double Piece::laneClearTime(const Lane& lane, double d, double v) const
{
    return (laneLength(lane) - d) / v;
}

double Piece::arcLength(const Lane& lane) const
{
    return laneRadius(lane) * std::abs(angle) * PI / 180.f;
}

double Piece::arcLength(double offCenterDistance) const
{
    return (radius - offCenterDistance) * std::abs(angle) * PI / 180.f;
}

Piece Piece::createStraight(double length, bool isSwitch)
{
    Piece piece = {Piece::TypeStraight, length, 0, 0, isSwitch};
    return piece;
}

Piece Piece::createBend(double radius, double angle, bool isSwitch)
{
    Piece piece = {Piece::TypeBend, 0, radius, angle, isSwitch};
    return piece;
}

Track& Track::operator<<(const Lane& lane)
{
    lanes.push_back(lane);
    return *this;
}

Track& Track::operator<<(const Piece& piece)
{
    pieces.push_back(piece);
    return *this;
}

} // namespace hwo
