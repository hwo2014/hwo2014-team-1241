#pragma once

namespace hwo
{

struct Time
{
    Time()
        : tick(0)
    {}
    Time(int tick)
        : tick(tick)
    {}

    int tick;

    Time operator+(int dt) const
    {
        return Time {tick + dt};
    }

    Time operator-(int dt) const
    {
        return Time {tick - dt};
    }
};

struct Race
{
    int lapCount;
    int durationMs;
    int maxLapTimeMs;
    bool quickRace;
};

} // namespace hwo
