@echo off
echo Building...
rem install Mingw from here
rem http://nuwen.net/mingw.html

set PL="cpp"
set BOTKEY="g9kK7u/0WGWQsQ"
set BOTNAME="ATK-autojengi"

g++ ^
    main.cpp ^
    protocol.cpp ^
    connection.cpp ^
    game_logic.cpp ^
    game_state.cpp ^
    car.cpp ^
    track.cpp ^
    -std=c++11 -I jsoncons/src/ -l boost_system -l ws2_32 -o plusbot
