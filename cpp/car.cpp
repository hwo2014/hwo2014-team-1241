#include "car.h"

#include <limits>
#include <iostream>

namespace hwo
{

const CarState* Car::findState(const Time& time) const
{
    for (const CarState& state : states)
        if (state.time.tick == time.tick)
            return &state;

    return 0;
}

bool Car::crashed() const
{
    std::deque<CarState>::const_iterator i = states.begin();
    while (i != states.end())
    {
        if (i->event == CarState::EventCrash)
            return true;
        if (i->event == CarState::EventSpawn)
            return false;
        ++i;
    }
    return false;
}

double Car::velocity(const Track& track, const Time& time) const
{
    // Use two adjacent states
    const CarState* s0 = findState(time);
    const CarState* s1 = findState(time - 1);

    // Return zero-velocity for non-existing states
    if (!s0 || !s1 || s0 == s1)
        return 0.0;

    if (s0->piecePos.index == s1->piecePos.index)
    {
        // Within single piece
        const double dd = s0->piecePos.distance - s1->piecePos.distance;
        const double dt = s0->time.tick - s1->time.tick;
        return dd / dt;
    }
    else
    if ((s0->piecePos.index == s1->piecePos.index + 1) ||
        (s0->piecePos.index == 0 && s1->piecePos.index == int(track.pieces.size()-1)))
    {
        // Within adjacent pieces
        const Lane& lane = track.lanes.at(s1->piecePos.laneStartIndex);
        const double ll  = track.pieces.at(s1->piecePos.index).laneLength(lane);
        const double d1  = ll - s1->piecePos.distance;
        const double dd  = s0->piecePos.distance + d1;
        const double dt  = s0->time.tick - s1->time.tick;
        return dd / dt;
    }

    // Non-adjacent pieces velocity not supported, for now
    return std::numeric_limits<double>::quiet_NaN();
}

double Car::acceleration(const Track& track, const Time& time) const
{
    // Use two adjacent velocities
    const int dt    = 1;
    const double v0 = velocity(track, time);
    const double v1 = velocity(track, time - dt);
    const double dv = v0 - v1;
    return dv / dt;
}

double Car::jerk(const Track& track, const Time& time) const
{
    // Use two adjacent accelerations
    const int dt    = 1;
    const double a0 = acceleration(track, time);
    const double a1 = acceleration(track, time - dt);
    const double av = a0 - a1;
    return av / dt;
}

double Car::angularVelocity(const Time& time) const
{
    // Use two adjacent states
    const CarState* s0 = findState(time);
    const CarState* s1 = findState(time - 1);

    // Return zero-velocity for non-existing states
    if (!s0 || !s1 || s0 == s1)
        return 0.0;

    const double da = s0->angle - s1->angle;
    const double dt = s0->time.tick - s1->time.tick;
    return da / dt;
}

double Car::angularAcceleration(const Time& time) const
{
    // Use two adjacent angular velocities
    const int dt     = 1;
    const double av0 = angularVelocity(time);
    const double av1 = angularVelocity(time - dt);
    const double dav = av0 - av1;
    return dav / dt;
}

double Car::centripetalForce(const Track& track, const Time& time) const
{
    if (const CarState* state = findState(time))
    {
        const Lane&  l = track.lanes.at(state->piecePos.laneStartIndex);
        const Piece& p = track.pieces.at(state->piecePos.index);
        if (p.type == Piece::TypeBend)
        {
            const double r = p.laneRadius(l);
            const double v = velocity(track, time);
            return v * v / r;
        }
    }
    return 0.0;
}

double Car::estimatedVelocityMax(const Track& track) const
{
    for (int i = states.size() - 1; i >= 0; --i)
    {
        // Time on x-axis, velocity on y-axis
        const CarState& state = states.at(i);
        const Time time = state.time;
    }
    return 0.0;
}

Car& Car::updatePosition(const CarState& state)
{
    states.push_front(state);
    return *this;
}

Car& Car::updateEvent(const CarState& state)
{
    // If the state with matching time exists, update it
    if (states.size() > 0 && states.front().time.tick == state.time.tick)
        states[0].event = state.event;
    else
        // Add a new state
        states.push_front(state);

    return *this;
}

} // namespace hwo
