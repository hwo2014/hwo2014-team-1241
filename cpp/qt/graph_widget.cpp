#ifdef QT_VISUALIZATION
#include "graph_widget.h"

#include <QtGui/QPainter>

namespace hwo
{

/*-----------------------------------------------------------------*/

GraphWidget::GraphWidget(int history, const QPen &pen, float min, float max)
    : history(history)
    , pen(pen)
    , min(min)
    , max(max)
{
    setMinimumSize(300, 100);

    values.resize(history, 0.0f);
}

/*-----------------------------------------------------------------*/

void GraphWidget::append(float value)
{
    min = qMin(min, value);
    max = qMax(max, value);
    values.push_back( value );
    values.pop_front();
    update();
}

/*-----------------------------------------------------------------*/

void GraphWidget::paintEvent(QPaintEvent * /*event*/)
{
    QRect r( rect() );
    r.adjusted(0,0,-1,-1);
    double stepX = double(r.width()) / history;

    QPolygonF poly;
    double stepY = 0;
    double scale = (max - min) * 1.05;
    if (scale != 0)
        stepY = double(r.height()) / scale;
    for (int x=0; x<history; x++)
    {
        double i = x * stepX;
        double j = (max - values[x]) * stepY;
        poly << QPointF(i,j);
    }

    QPainter painter(this);
    double zeroLine = (max-0)*stepY;
    double maxLine = (max-max)*stepY;
    double minLine = (max-min)*stepY;

    // Min and max lines
    painter.setPen( Qt::lightGray );
    painter.drawLine( r.left(),maxLine, r.right(),maxLine );
    painter.drawLine( r.left(),minLine, r.right(),minLine );

    // Min and max values
    painter.setPen( Qt::black );
    QFontMetrics metrics( painter.font() );
    painter.drawText( 5, maxLine + metrics.ascent(), QString::number(max) );
    painter.drawText( 5, minLine - metrics.descent(), QString::number(min) );

    // Graph borders
    painter.setPen( Qt::black );
    painter.drawLine( r.left(),zeroLine, r.right(),zeroLine );
    painter.drawLine( r.topLeft(), r.bottomLeft() );

    // Graph values
    painter.setPen( pen );
    painter.drawPolyline( poly );
}

} // namespace hwo
#endif
