#ifdef QT_VISUALIZATION
#include "game_widget.h"

#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QLabel>

#include "../game_state.h"
#include "track_widget.h"
#include "graph_widget.h"

#include <QtCore/QDebug>

namespace hwo
{

/*-----------------------------------------------------------------*/

GameWidget::GameWidget()
    : gameState(nullptr)
{
}

/*-----------------------------------------------------------------*/

void GameWidget::setState(const GameState *state)
{
    gameState = state;
    carGraphs.clear();

    QHBoxLayout *layout = new QHBoxLayout;
    {
        trackWidget = new TrackWidget;
        trackWidget->setState(state);
        layout->addWidget( trackWidget );

        QVBoxLayout *graphLayout = new QVBoxLayout;
        foreach (const Car &car, gameState->cars)
        {
            QLabel *label = new QLabel( car.id.name.c_str() );
            label->setStyleSheet( QString("QLabel { color : %1; }").arg(car.id.color.c_str()) );
            graphLayout->addWidget( label );

            CarGraphs graphs;
            int history = 2000;
            graphs.throttle = new GraphWidget(history, QPen(Qt::red), 0.0, 1.0);
            graphLayout->addWidget( new QLabel("Throttle") );
            graphLayout->addWidget( graphs.throttle );

            graphs.velocity = new GraphWidget(history, QPen(Qt::blue), 0.0, 9.0);
            graphLayout->addWidget( new QLabel("Velocity") );
            graphLayout->addWidget( graphs.velocity );

            graphs.acceleration = new GraphWidget(history, QPen(Qt::blue));
            graphLayout->addWidget( new QLabel("Acceleration") );
            graphLayout->addWidget( graphs.acceleration );

            graphs.angle = new GraphWidget(history, QPen(Qt::blue), -10, 10);
            graphLayout->addWidget( new QLabel("Angle") );
            graphLayout->addWidget( graphs.angle );

            carGraphs.insert(car.id.name.c_str(), graphs);
        }
        graphLayout->addStretch(0);
        layout->addLayout( graphLayout );
    }
    setLayout(layout);

    // Set window title
    setWindowTitle( state->track.name.c_str() );
    resize( sizeHint() );
    update();
}

/*-----------------------------------------------------------------*/

void GameWidget::update()
{
    foreach (const Car &car, gameState->cars)
    {
        QMap<QString,CarGraphs>::iterator graphs = carGraphs.find( car.id.name.c_str() );
        if (graphs == carGraphs.end())
            continue;

        CarState state = car.states.front();
        Time time = state.time;
        graphs->throttle->append( state.throttle );
        graphs->velocity->append( car.velocity( gameState->track, time ) );
        double acc = car.acceleration( gameState->track, time );
        if (car.crashed())
            acc = 0;
        graphs->acceleration->append( acc );
        graphs->angle->append( state.angle );
    }

    QWidget::update();
}

} // namespace hwo
#endif
