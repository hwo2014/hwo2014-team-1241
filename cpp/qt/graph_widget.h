#pragma once
#ifdef QT_VISUALIZATION

/*-----------------------------------------------------------------*/

#include <QtWidgets/QWidget>
#include <QtGui/QPen>
#include <deque>

/*-----------------------------------------------------------------*/

namespace hwo
{

class GraphWidget : public QWidget
{
public:
    GraphWidget(int history, const QPen &pen,
                float min = 0.0f, float max = 0.0f);

    void append(float value);

protected:
    void paintEvent(QPaintEvent *event);

private:
    int history;
    QPen pen;

    float min,max;
    std::deque<float> values;
};

} // namespace hwo

#endif
