#pragma once
#ifdef QT_VISUALIZATION

/*-----------------------------------------------------------------*/

#include <QtWidgets/QWidget>
#include <QtGui/QVector3D>
#include "../race.h"
#include "../track.h"
#include "../car.h"

/*-----------------------------------------------------------------*/

namespace hwo
{
class GameState;

class TrackWidget : public QWidget
{
public:
    TrackWidget();

    void setState(const GameState *state);

    QSize sizeHint(void) const;

protected:
    void paintEvent(QPaintEvent *event);

private:
    const GameState *gameState;
    double minLaneDistance;
    double maxLaneDistance;
    double laneAverageDistance;
    QRectF trackRect;

    double displayScale;
    const QVector3D front;
    const QVector3D right;
    const QVector3D up;

    QVector3D lanePos(const QVector3D &center, const double &angle, double laneDistance);
    QVector3D straightPos(QVector3D *center, double *angle, double length, double laneDistance);
    QVector3D bendPos(QVector3D *center, double *angle, const Piece &piece, double length, double laneDistance, QVector3D *origo);

    // Make path for distance from center
    QPainterPath makePath(double distance);

    void drawCar(QPainter *painter, const Car &car);
};

} // namespace hwo

#endif
