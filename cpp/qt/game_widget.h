#pragma once
#ifdef QT_VISUALIZATION

/*-----------------------------------------------------------------*/

#include <QtWidgets/QWidget>
#include <QtCore/QMap>

/*-----------------------------------------------------------------*/

namespace hwo
{
class GameState;
class TrackWidget;
class GraphWidget;

class GameWidget : public QWidget
{
public:
    GameWidget();

    void setState(const GameState *state);

    void update(void);

private:
    const GameState *gameState;
    TrackWidget *trackWidget;

    struct CarGraphs
    {
        GraphWidget *throttle;
        GraphWidget *velocity;
        GraphWidget *acceleration;
        GraphWidget *angle;
    };
    QMap<QString,CarGraphs> carGraphs;
};

} // namespace hwo

#endif
