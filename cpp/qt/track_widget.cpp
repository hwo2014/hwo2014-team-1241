#ifdef QT_VISUALIZATION
#include "track_widget.h"

#include <QtGui/QPainter>
#include <QtGui/QQuaternion>
#include <QtCore/QDebug>

#include "../game_state.h"

namespace hwo
{

/*-----------------------------------------------------------------*/

TrackWidget::TrackWidget()
    : gameState(nullptr)
    , displayScale(0.5)
    , front(1,0,0)
    , right(0,1,0)
    , up(0,0,1)
{
}

/*-----------------------------------------------------------------*/

void TrackWidget::setState(const GameState *state)
{
    gameState = state;

    // Precalculate some data

    // Find min and max lane distance and average distance between lanes
    minLaneDistance = 0;
    maxLaneDistance = 0;
    laneAverageDistance = 0;
    std::vector<Lane>::const_iterator lane = gameState->track.lanes.begin();
    while (lane != gameState->track.lanes.end())
    {
        minLaneDistance = qMin(minLaneDistance, lane->offCenterDistance);
        maxLaneDistance = qMax(maxLaneDistance, lane->offCenterDistance);

        std::vector<Lane>::const_iterator next = lane;
        ++next;
        if (next != gameState->track.lanes.end())
            laneAverageDistance += qAbs(lane->offCenterDistance - next->offCenterDistance);
        lane = next;
    }
    if (gameState->track.lanes.size() > 1)
        laneAverageDistance /= gameState->track.lanes.size()-1;

    // Calculate track size
    QRectF maxPath = makePath(maxLaneDistance).boundingRect();
    QRectF minPath = makePath(minLaneDistance).boundingRect();
    trackRect = maxPath | minPath;

    setMinimumSize( sizeHint() );
}

/*-----------------------------------------------------------------*/

QSize TrackWidget::sizeHint(void) const
{
    QSizeF scaledSize = trackRect.size() * displayScale;
    return QSize(scaledSize.width()+50, scaledSize.height()+50);
}

/*-----------------------------------------------------------------*/
// Calculate starting position
//
QVector3D TrackWidget::lanePos(const QVector3D &center, const double &angle, double laneDistance)
{
    QQuaternion q = QQuaternion::fromAxisAndAngle(up, angle);
    QVector3D s = q.rotatedVector( right );
    return center + s*laneDistance;
}

/*-----------------------------------------------------------------*/

QVector3D TrackWidget::straightPos(QVector3D *center, double *angle, double length, double laneDistance)
{
    QQuaternion q = QQuaternion::fromAxisAndAngle(up, *angle);
    QVector3D v = q.rotatedVector( front );
    QVector3D s = q.rotatedVector( right );

    *center += v * length;
    return *center + s*laneDistance;
}

/*-----------------------------------------------------------------*/

QVector3D TrackWidget::bendPos(QVector3D *center, double *angle, const Piece &piece, double length, double laneDistance, QVector3D *origo)
{
    double radius = piece.radius;
    double d = -laneDistance;
    if (piece.angle < 0)
    {
        radius = -radius;
        d = -d;
    }

    // Calculate arc origo
    QQuaternion q = QQuaternion::fromAxisAndAngle(up, *angle);
    QVector3D s = q.rotatedVector( right );
    *origo = *center + s * radius;

    // Arc end point
    double arcLenght = piece.arcLength(laneDistance);
    double diff = length / arcLenght;
    QQuaternion qEnd = QQuaternion::fromAxisAndAngle(up, *angle + piece.angle);
    QQuaternion q2 = QQuaternion::slerp(q, qEnd, diff);
    QVector3D s2 = q2.rotatedVector( right );

    *center = *origo - s2 * radius;
    *angle += piece.angle * diff;
    return *center + s2 * laneDistance;
}

/*-----------------------------------------------------------------*/
// Make path for distance from center
//
QPainterPath TrackWidget::makePath(double laneDistance)
{
    QVector3D center(0,0,0);
    double angle = 0;

    QPainterPath path;
    {
        // Starting position
        QVector3D p = lanePos(center, angle, laneDistance);
        path.moveTo(p.x(), p.y());
    }
    foreach (const Piece &piece, gameState->track.pieces)
    {
        if (piece.type == Piece::TypeStraight)
        {
            QVector3D p = straightPos(&center, &angle, piece.length, laneDistance);
            path.lineTo(p.x(), p.y());
        }
        else if (piece.type == Piece::TypeBend)
        {
            QVector3D origo;
            double originalAngle = angle;
            bendPos(&center, &angle, piece, piece.arcLength(laneDistance), laneDistance, &origo);

            if (piece.angle > 0)
            {
                double d = piece.radius - laneDistance;
                path.arcTo( origo.x()-d, origo.y()-d,
                            d*2, d*2,
                            90-originalAngle, -piece.angle );
            }
            else
            {
                double d = piece.radius + laneDistance;
                path.arcTo( origo.x()-d, origo.y()-d,
                            d*2, d*2,
                            (90+180)-originalAngle, -piece.angle );
            }
        }
    }
    path.closeSubpath();
    return path;
}

/*-----------------------------------------------------------------*/

void TrackWidget::drawCar(QPainter *painter, const Car &car)
{
    if (car.states.empty())
        return;

    QVector3D center(0,0,0);
    double angle = 0;
    const CarState state = car.states.front();
    double laneDistance = (
        gameState->track.lanes[state.piecePos.laneStartIndex].offCenterDistance +
        gameState->track.lanes[state.piecePos.laneEndIndex].offCenterDistance ) / 2.0;

    // Calculate car cordinates on track
    QVector3D origo;
    for (int index = 0; index < state.piecePos.index; index++)
    {
        const Piece &piece( gameState->track.pieces[index] );
        if (piece.type == Piece::TypeStraight)
            straightPos(&center, &angle, piece.length, laneDistance);
        else if (piece.type == Piece::TypeBend)
            bendPos(&center, &angle, piece, piece.arcLength(laneDistance), laneDistance, &origo);
    }
    {
        const Piece &piece( gameState->track.pieces[state.piecePos.index] );
        if (piece.type == Piece::TypeStraight)
            straightPos(&center, &angle, state.piecePos.distance, laneDistance);
        else if (piece.type == Piece::TypeBend)
            bendPos(&center, &angle, piece, state.piecePos.distance, laneDistance, &origo);
    }

    // Move to current lane and set our angle
    QVector3D pos = lanePos(center, angle, laneDistance);
    angle += state.angle;


    painter->save();

    // Move to car position and angle
    painter->translate(pos.x(), pos.y());
    painter->rotate( 90 + angle);

    // Draw the car with given color
    QColor color( car.id.color.c_str() );

    // Draw with transparent color when crashed
    if (car.crashed())
        color.setAlphaF(0.375);

    painter->setPen( color.darker(150) );
    painter->setBrush( color );
    painter->drawRect( -car.dimensions.width / 2.0,
                       -car.dimensions.guideFlagPosition,
                       car.dimensions.width,
                       car.dimensions.length );

    // Draw quide position
    QPen pen( color.darker(200) );
    pen.setWidth( 10 );
    painter->setPen( pen );
    painter->drawPoint( 0,0 );

    painter->restore();
}

/*-----------------------------------------------------------------*/

void TrackWidget::paintEvent(QPaintEvent * /*event*/)
{
    if (!gameState)
        return;

    QPainter p(this);
    p.fillRect( rect(), QBrush(Qt::lightGray) );

    // Center the track to widget
    p.translate(rect().center().x(), rect().center().y());
    p.scale(displayScale, displayScale);
    p.translate(-trackRect.center().x(), -trackRect.center().y());

    // Draw track area
    p.setPen( Qt::black );
    p.setBrush( Qt::darkGray );
    p.drawPath( makePath( minLaneDistance - laneAverageDistance) );
    p.setBrush( Qt::lightGray );
    p.drawPath( makePath( maxLaneDistance + laneAverageDistance) );

    // Draw all lanes
    QPen lanePen( Qt::gray );
    lanePen.setWidth( 5 );
    p.setPen( lanePen );
    p.setBrush( Qt::NoBrush );
    foreach (const Lane &lane, gameState->track.lanes)
    {
        p.drawPath( makePath( lane.offCenterDistance ) );
    }

    // Draw lines between track pieces
    QVector3D center(0,0,0);
    double angle = 0;
    p.setPen( Qt::black );
    p.setBrush( Qt::NoBrush );
    foreach (const Piece &piece, gameState->track.pieces)
    {
        QVector3D oldCenter = center;
        double oldAngle = angle;
        QVector3D origo;
        if (piece.type == Piece::TypeStraight)
            straightPos(&center, &angle, piece.length, 0);
        else if (piece.type == Piece::TypeBend)
            bendPos(&center, &angle, piece, piece.arcLength(0), 0, &origo);

        QVector3D a = lanePos(center, angle, minLaneDistance - laneAverageDistance);
        QVector3D b = lanePos(center, angle, maxLaneDistance + laneAverageDistance);
        p.drawLine(a.x(), a.y(), b.x(), b.y());

        if (piece.isSwitch)
        {
            p.setPen( lanePen );
            QVector3D a0 = lanePos(oldCenter, oldAngle, minLaneDistance);
            QVector3D b0 = lanePos(oldCenter, oldAngle, maxLaneDistance);
            QVector3D a1 = lanePos(center, angle, minLaneDistance);
            QVector3D b1 = lanePos(center, angle, maxLaneDistance);
            p.drawLine(a0.x(),a0.y(), b1.x(),b1.y());
            p.drawLine(b0.x(),b0.y(), a1.x(),a1.y());

            p.setPen( Qt::black );
        }
    }

    // Draw finish line
    {
        QVector3D a = lanePos( QVector3D(0,0,0), 0, minLaneDistance - laneAverageDistance*2 );
        QVector3D b = lanePos( QVector3D(0,0,0), 0, maxLaneDistance + laneAverageDistance*2 );
        QPen pen( Qt::yellow );
        pen.setWidth( 6 );
        p.setPen( pen );
        p.drawLine(a.x(), a.y(), b.x(), b.y());
    }

    // Draw all cars
    foreach (const Car &car, gameState->cars)
    {
        drawCar(&p, car);
    }
}

} // namespace hwo
#endif
