#pragma once

#include <vector>
#include <string>

namespace hwo
{

struct Lane
{
    // Index
    int index;

    // Off-center distance, negative to left, positive to right
    double offCenterDistance;
};

struct Piece
{
    // TODO: Could be implicitly asked via method (length > 0 for straights).
    enum Type
    {
        TypeStraight,
        TypeBend
    };

    // Piece type
    Type type;

    // Piece lenght, only valid for TypeStraight and TypeSwitch
    double length;

    // Piece radius and angle (degrees), only valid for TypeBend
    double radius;
    double angle;

    // Piece switch flag
    bool isSwitch;

    // Piece length for given lane, all piece types supported.
    double laneLength(const Lane& lane) const;

    // Piece radius for given lane
    double laneRadius(const Lane& lane) const;

    // Piece remaining clear time for given lane,
    // piece distance and velocity, in ticks
    double laneClearTime(const Lane& lane, double d, double v) const;

    // Arc length for given lane, only valid for TypeBend
    double arcLength(const Lane& lane) const;
    double arcLength(double offCenterDistance) const;

    // Factory methods for creating pieces
    static Piece createStraight(double length, bool isSwitch = false);
    static Piece createBend(double radius, double angle, bool isSwitch = false);
};

struct PiecePosition
{
    // Piece index
    int index;

    // In-piece-distance, measured from car's guide flag
    double distance;

    // Lane indices, values differ only when car is switching lanes
    int laneStartIndex;
    int laneEndIndex;

    // Laps complete, -1 when the first lap has not been started yet
    int lap;
};

// TODO: Add queries for piece-to-piece distance, etc.
struct Track
{
    // Track ID and name
    std::string id;
    std::string name;

    // Track lanes
    std::vector<Lane> lanes;

    // Track pieces
    std::vector<Piece> pieces;

    Track& operator<<(const Lane& lane);
    Track& operator<<(const Piece& piece);
};

} // namespace hwo
