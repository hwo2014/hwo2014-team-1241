#pragma once

#include <string>
#include <iostream>
#include <jsoncons/json.hpp>
#include <map>

#include "track.h"
#include "car.h"
#include "race.h"

namespace hwo
{

struct GameState
{
    Time time;
    Race race;
    Track track;
    std::vector<Car> cars;

    std::map<int, double> radiusVsVelocity;
    Car* findCar(const CarId& id);

    Car* findCar(const std::string& name);

    // Update game state, msg is of msgType carPositions
    GameState& update(const jsoncons::json& msg);

    // Update game state, msg is of msgType spawn
    GameState& spawn(const jsoncons::json& msg);

    // Update game state, msg is of msgType crash
    GameState& crash(const jsoncons::json& msg);

    void updateVelocityMemory(Car *car, bool force=false);
    double throttleFromVelocityMemory(Car *car);
};

// TODO: Move these functions to a separate file
CarId createCarId(const jsoncons::json& data);
GameState createGameState(const jsoncons::json& data);

} // namespace hwo
