#include <string>

#include "game_state.h"

namespace hwo
{

Car* GameState::findCar(const CarId& id)
{
    for (Car& car : cars)
        if (car.id.name == id.name)
            return &car;

    return 0;
}

Car* GameState::findCar(const std::string& name)
{
    return findCar(CarId {name});
}

GameState& GameState::update(const jsoncons::json& msg)
{
    // Game tick
    time = Time( msg.get("gameTick", 0).as<int>() );

    // Car positions
    const jsoncons::json carsData = msg["data"];
    const int carCount = carsData.size();
    for (int i = 0; i < carCount; ++i)
    {
        const jsoncons::json carData = carsData[i];
        CarId id = createCarId(carData["id"]);
        if (Car* car = findCar(id))
        {
            const jsoncons::json posData  = carData["piecePosition"];
            const jsoncons::json laneData = posData["lane"];

            const int pieceIndex  = posData["pieceIndex"].as<int>();
            const double distance = posData["inPieceDistance"].as<double>();
            const int lap         = posData["lap"].as<int>();
            const int laneStart   = laneData["startLaneIndex"].as<int>();
            const int laneEnd     = laneData["endLaneIndex"].as<int>();

            PiecePosition piecePos =
                {pieceIndex, distance, laneStart, laneEnd, lap};

            const double angle = carData["angle"].as<double>();

            CarState state = {time, piecePos, angle};
            car->updatePosition(state);

#if 0
            // Bend analysis, WIP.
            const Lane&  l = track.lanes.at(laneStart);
            const Piece& p = track.pieces.at(pieceIndex);

            if (p.type == Piece::TypeBend)
            {
                // In a bend currently, gather information.
                const double v = car->velocity(track, time);
                const double len = p.laneLength(l);
                const double r = p.laneRadius(l);
                const double t = p.laneClearTime(l, 0, v);
                const double c = t * (v * v / r);

                std::cout << " t " << time.tick
                          << " p " << pieceIndex
                          << " slip " << angle
                          << " v " << v
                          << " c " << c
                          << " l " << len
                          << " r " << r
                          << " t " << t
                          << std::endl;
            }
#endif
        }
    }

    return *this;
}

GameState& GameState::spawn(const jsoncons::json& msg)
{
    Car* car = findCar(msg["data"]["name"].as<std::string>());
    CarState state = {};
    state.event = CarState::EventSpawn;
    car->updateEvent(state);
    return *this;
}

GameState& GameState::crash(const jsoncons::json& msg)
{
    Car* car = findCar(msg["data"]["name"].as<std::string>());
    CarState state = {};
    state.event = CarState::EventCrash;
    car->updateEvent(state);
    return *this;
}

CarId createCarId(const jsoncons::json& data)
{
    CarId id = {data["name"].as<std::string>(),
                data["color"].as<std::string>()};

    return id;
}

GameState createGameState(const jsoncons::json& data)
{
    const jsoncons::json trackData  = data["race"]["track"];
    const jsoncons::json lanesData  = trackData["lanes"];
    const jsoncons::json piecesData = trackData["pieces"];
    const jsoncons::json carsData   = data["race"]["cars"];
    const jsoncons::json raceData   = data["race"]["raceSession"];

    // Track lanes
    const int laneCount = lanesData.size();
    std::vector<Lane> lanes(laneCount);
    for (int i = 0; i < laneCount; ++i)
    {
        const jsoncons::json laneData = lanesData[i];
        Lane lane = {laneData["index"].as<int>(),
                     laneData["distanceFromCenter"].as<double>()};

        lanes[lane.index] = lane;
    }

    // Track pieces
    const int pieceCount = piecesData.size();
    std::vector<Piece> pieces;
    for (int i = 0; i < pieceCount; ++i)
    {
        const jsoncons::json pieceData = piecesData[i];
        const double length = pieceData.get("length", 0.0).as<double>();
        const double radius = pieceData.get("radius", 0.0).as<double>();
        const double angle  = pieceData.get("angle",  0.0).as<double>();
        const bool isSwitch = pieceData.get("switch", false).as<bool>();

        Piece piece = {length > 0.0 ? Piece::TypeStraight : Piece::TypeBend,
                       length, radius, angle, isSwitch};

        std::cout << __FUNCTION__ << " piece " << i << " "
                  << piece.type << " " << piece.length << " "
                  << piece.radius << " " << piece.angle << " "
                  << piece.isSwitch << std::endl;

        pieces.push_back(piece);
    }

    // Create track
    Track track = {trackData["id"].as<std::string>(),
                   trackData["name"].as<std::string>(),
                   lanes, pieces};

    // Participating cars
    const int carCount = carsData.size();
    std::vector<Car> cars;
    for (int i = 0; i < carCount; ++i)
    {
        const jsoncons::json carData = carsData[i];
        const jsoncons::json idData  = carData["id"];
        const jsoncons::json dimData = carData["dimensions"];

        CarId id = createCarId(idData);
        CarDimensions dimensions = {dimData["width"].as<double>(),
                                    dimData["length"].as<double>(),
                                    dimData["guideFlagPosition"].as<double>()};

        Car car = {id, dimensions};

        std::cout << __FUNCTION__ << " car "
                  << car.id.name << " " << car.id.color << " "
                  << car.dimensions.width << " " << car.dimensions.length << " "
                  << car.dimensions.guideFlagPosition << std::endl;

        cars.push_back(car);
    }

    // Race session
    const int lapCount     = raceData.get("laps", 0).as<int>();
    const int durationMs   = raceData.get("durationMs", 0).as<int>();
    const int maxLapTimeMs = raceData["maxLapTimeMs"].as<int>();
    const bool quickRace   = raceData["quickRace"].as<bool>();
    Race race = {lapCount, durationMs, maxLapTimeMs, quickRace};

    // Create game state
    GameState state = {Time(), race, track, cars};

    // Initialize radius vs velocity history
    std::vector<double> throttleVsAngle;
    throttleVsAngle.resize(11, -1.0);
    for (std::vector<Lane>::iterator lane = track.lanes.begin(); lane != track.lanes.end(); ++lane)
        for (std::vector<Piece>::iterator piece = track.pieces.begin(); piece != track.pieces.end(); ++piece)
            if (piece->type == Piece::TypeBend)
            {
                int radius = fabs( piece->laneRadius(*lane) );
                double defaultVelocity = radius / 30.0;
                state.radiusVsVelocity.insert( std::pair<int,double>(radius, defaultVelocity) );
            }

    return state;
}


void GameState::updateVelocityMemory(Car *car, bool force)
{
    if (car->states.size() < 2)
        return;
    CarState s0 = car->states[0];
    CarState s1 = car->states[1];
    if (s0.piecePos.index == s1.piecePos.index && !force)
        return;

    // Calculate current lane radius
    PiecePosition pos = s1.piecePos;
    Piece piece = track.pieces[ pos.index ];
    if (piece.type == Piece::TypeStraight)
        return;
    Lane lane = track.lanes[ pos.laneEndIndex ];
    int radius = fabs( piece.laneRadius( lane ) );

    // Store velocity data for this radius
    double velocity = car->velocity(track,s0.time);
    std::map<int, double>::iterator i2 = radiusVsVelocity.find( radius );
    if (i2->second < 0)
        i2->second = velocity;
    i2->second = (i2->second*3 + velocity) / 4.0;

    std::cout << "Radius " << radius
              << " velocity = " << i2->second << std::endl;
}

double GameState::throttleFromVelocityMemory(Car *car)
{
    if (car->states.empty())
        return 1.0;
    CarState state = car->states.front();

    // Check possible velocities for few pieces
    PiecePosition pos = state.piecePos;
    Lane lane = track.lanes[ pos.laneEndIndex ];
    double throttle = 0;
    int count = 0;
    int maxDistance = 6;
    for (int c=0; c<maxDistance; c++)
    {
        unsigned index = pos.index + c;
        if (index > track.pieces.size())
            index -= track.pieces.size();
        Piece piece = track.pieces[ index ];
        double t = 1;
        if (piece.type == Piece::TypeBend)
        {
            int radius = fabs( piece.laneRadius( lane ) );
            std::map<int,double>::iterator i = radiusVsVelocity.find( radius );
            if (i != radiusVsVelocity.end())
                if (i->second > 0)
                    // TODO: convert velocity to throttle
                    t = i->second / 10.0;
        }

        throttle += t * (maxDistance - c);
        count += (maxDistance - c);
    }

    return throttle / count;
}

} // namespace hwo
