#include "game_logic.h"
#include "protocol.h"

#ifdef QT_VISUALIZATION
#include"qt/game_widget.h"
#endif

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "lapFinished", &game_logic::on_lap_finish },
      { "finish", &game_logic::on_finish },
      { "tournamentEnd", &game_logic::on_tournament_end },
      { "joinRace", &game_logic::on_join_race },
      { "dnf", &game_logic::on_dnf },
      { "spawn", &game_logic::on_spawn }
    }
{
#ifdef QT_VISUALIZATION
  widget = new hwo::GameWidget;
  widget->show();
#endif
}

game_logic::~game_logic()
{
#ifdef QT_VISUALIZATION
  delete widget;
#endif
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, msg);
  }
  else
  {
    std::cout << "Message not processed: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& msg)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& msg)
{
  std::cout << "Your car informed" << std::endl;
  carId = hwo::createCarId(msg["data"]);
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& msg)
{
  std::cout << "Game init, track "
            << msg["data"]["race"]["track"]["name"].as<std::string>()
            << ", "
            << msg["data"]["race"]["raceSession"]["laps"].as<std::string>()
            << " laps" << std::endl;
  gameState = hwo::createGameState(msg["data"]);
#ifdef QT_VISUALIZATION
  widget->setState( &gameState );
#endif
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& msg)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& msg)
{
  // Update game state
  gameState.update(msg);

  // Apply car handling logic here, out comes used throttle value
  double throttle = 0.65;

  // Update car state with throttle
  // TODO: Clean this up with a setter method
  if (hwo::Car* car = gameState.findCar(carId))
  {
      throttle = gameState.throttleFromVelocityMemory(car);
      if (car->states.size() > 0)
          car->states.front().throttle = throttle;
      gameState.updateVelocityMemory( car );
  }

#ifdef QT_VISUALIZATION
  widget->update();
#endif
  return { make_throttle(throttle, gameState.time.tick) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& msg)
{
  std::cout << msg["data"]["name"].as<std::string>()
            << " crashed" << std::endl;
  gameState.crash(msg);
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& msg)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finish(const jsoncons::json& msg)
{
    std::cout   << "Car "
                << msg["data"]["car"]["name"].as<std::string>()
                << " finished lap "
                << msg["data"]["lapTime"]["lap"].as<std::string>()
                << " in (ms): "
                << msg["data"]["lapTime"]["millis"].as<std::string>()
                << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& msg)
{
    std::cout   << "Car " << msg["data"]["name"].as<std::string>()
                << " finished" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& msg)
{
    std::cout << "Tournament finished" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_join_race(const jsoncons::json& msg)
{
    std::cout   << "Car " << msg["data"]["botId"]["name"].as<std::string>()
                << " joined" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& msg)
{
    std::cout   << "Car " << msg["data"]["car"]["name"].as<std::string>()
                << " disqualified" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& msg)
{
    std::cout   << msg["data"]["name"].as<std::string>()
                << " spawned" << std::endl;
    gameState.spawn(msg);
    return { make_ping() };
}
