#include "track_gen.h"

namespace hwo
{
namespace TrackGen
{

Track createTechSpecExample()
{
    const Lane lane1 = {-10};
    const Lane lane2 = { 10};

    Track track;
    track << lane1 << lane2;

    track << Piece::createStraight(75)
          << Piece::createStraight(25, true)
          << Piece::createBend(50, 180)
          << Piece::createBend(50, -90, true)
          << Piece::createBend(50,  90)
          << Piece::createStraight(25, true);
    // TODO: rest of the track

    return track;
}

} // namespace TrackGen
} // namespace
