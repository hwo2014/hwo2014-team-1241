#pragma once

#include <utility>
#include <vector>
#include <iostream>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>

namespace hwo
{

template <typename T>
struct Polynomial
{
    typedef std::pair<T, T> Data;

    T valueAt(T x) const
    {
        T y  = 0;
        T xt = 1;
        for (int i = 0; i < coeff.size(); ++i)
        {
            y  += coeff[i] * xt;
            xt *= x;
        }
        return y;
    }

    Polynomial& fit(const std::vector<Data>& data, int degree)
    {
        using namespace boost::numeric::ublas;

        // Create x and y matrices
        const int dataSize = data.size();
        matrix<T> matrixX(dataSize, degree);
        matrix<T> matrixY(dataSize, 1);

        // Fill y matrix
        for (int i = 0; i < dataSize; ++i)
            matrixY(i, 0) = data.at(i).second;

        // Fill x matrix
        for (int row = 0; row < dataSize; ++row)
        {
            T val = T(1.0);
            for (int col = 0; col < degree; ++col)
            {
                matrixX(row, col) = val;
                val *= data.at(row).first;
            }
        }

        // Transpose x matrix
        const matrix<T> matrixXT(trans(matrixX));

        // Multiply with original X matrix
        matrix<T> matrixXTMX(prec_prod(matrixXT, matrixX));

        // Multiply with Y matrix
        matrix<T> matrixXTMY(prec_prod(matrixXT, matrixY));

        // LU decomposition
        permutation_matrix<int> perm(matrixXTMX.size1());
        const std::size_t singular = lu_factorize(matrixXTMX, perm);

        // LU back-substitution
        lu_substitute(matrixXTMX, perm, matrixXTMY);

        // Store coefficients
        coeff = std::vector<T>(matrixXTMY.data().begin(),
                               matrixXTMY.data().end());

        return *this;
    }

    std::vector<T> coeff;
};

} // namespace hwo
