#pragma once

#include "track.h"

namespace hwo
{
namespace TrackGen
{

// Creates the example track shown in the HWO techspec:
// https://helloworldopen.com/techspec
Track createTechSpecExample();

} // namespace TrackGen
} // namespace
