#pragma once

#include <utility>
#include <deque>

#include "race.h"
#include "track.h"

namespace hwo
{

struct CarId
{
    // Car name and color
    std::string name;
    std::string color;
};

struct CarDimensions
{
    // Car width and length
    double width;
    double length;

    // Car guide flag position, distance from the nose of the car
    double guideFlagPosition;
};

struct CarState
{
    enum Event
    {
        EventNone,
        EventSpawn,
        EventCrash
    };

    // Time
    Time time;

    // In-piece position
    PiecePosition piecePos;

    // Car slip angle, degrees,
    // positive to right, negative to left from center line
    //
    // Note: The slip angle does NOT affect the axis of thrust,
    //       which remains aligned with the lane the car resides on.
    double angle;

    // Car throttle, range [0...1]
    double throttle;

    // Event, optional
    Event event;
};

// TODO: Add queries for finding out implicit params, such as velocity, etc.
struct Car
{
    // Car identity
    CarId id;

    // Car dimensions
    CarDimensions dimensions;

    // Car state history, latest item first, states.front()
    std::deque<CarState> states;

    // Car state for given time or null if no state found
    const CarState* findState(const Time& time) const;

    // State of the car, true if car is currently crashed
    bool crashed() const;

    // Car velocity on given track at given time,
    // based on position history.
    //
    // Unit is piece-distance / ticks.
    double velocity(const Track& track, const Time& time) const;

    // Car acceleration on given track at given time,
    // based on velocity history.
    //
    // Unit is piece-distance / ticks ^ 2.
    double acceleration(const Track& track, const Time& time) const;

    // Car jerk (accelaration derivative) on given track at given time,
    // based on acceleration history.
    double jerk(const Track& track, const Time& time) const;

    // Car angular velocity at given time,
    // based on rotation history.
    //
    // Unit is rotation (degrees) / ticks.
    double angularVelocity(const Time& time) const;

    // Car angular acceleration at given time,
    // based on angular velocity history.
    //
    // Unit is rotation (degrees) / ticks ^ 2.
    double angularAcceleration(const Time& time) const;

    double centripetalForce(const Track& track, const Time& time) const;

    // TODO: Find another way to implement this
    double estimatedVelocityMax(const Track& track) const;

    // Update car position
    Car& updatePosition(const CarState& state);

    // Update car event
    Car& updateEvent(const CarState& state);
};

} // namespace hwo
