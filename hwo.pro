TARGET = hwo2014

QT += widgets
CONFIG += console
CONFIG -= windows

#QMAKE_LFLAGS += -F/Library/Frameworks
QMAKE_LFLAGS += $$QMAKE_LFLAGS_WINDOW
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -Wall -pedantic

DEFINES += QT_VISUALIZATION

# Store objects to temp
unix:OBJECTS_DIR = /tmp/qt-$$TARGET

# Include root path
INCLUDEPATH = .
TEMPLATE = app

# Source files
SOURCES += \
    cpp/main.cpp \
    cpp/protocol.cpp \
    cpp/connection.cpp \
    cpp/game_logic.cpp \
    cpp/game_state.cpp \
    cpp/car.cpp \
    cpp/track.cpp \
    cpp/qt/game_widget.cpp \
    cpp/qt/track_widget.cpp \
    cpp/qt/graph_widget.cpp


# Header files
HEADERS += \
    cpp/protocol.h \
    cpp/connection.h \
    cpp/game_logic.h \
    cpp/game_state.h \
    cpp/car.h \
    cpp/track.h \
    cpp/race.h \
    cpp/qt/game_widget.h \
    cpp/qt/track_widget.h \
    cpp/qt/graph_widget.h

# UI Forms
FORMS += \

# Other
OTHER_FILES += \

# Add libraries
win32 {
    INCLUDEPATH += cpp/jsoncons/src/
    LIBS += -lws2_32

    INCLUDEPATH += C:/Code/SDK/boost_1_55_0
    LIBS += -LC:/Code/SDK/boost_1_55_0/stage/lib
    LIBS += -lboost_system-mgw48-mt-1_55
}
unix {
    INCLUDEPATH += cpp/jsoncons/src/
    LIBS += -lpthread -lboost_system
}

